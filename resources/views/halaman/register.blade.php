@extends('layout.master')
@section('judul')
    Form Sign Uo
@endsection
 
@section('content')
        <h1><b>Buat Account Baru !</b></h1>
        <h3><b>Sign Up Form</b></h3>
    <form action="/welcome" method="post">
	@csrf
      <label for="fname">First name : <br></label><br>
      <input type="text" id="fname" name="fname"><br>
      <label for="lname"><br>Last name : <br></label><br>
      <input type="text" id="lname" name="lname"><br><br>
      <p>Gender:</p>
    
        <input type="radio" name="sign_up" value="Male">
        <label for="Male">Male</label><br>
        <input type="radio" name="sign_up" value="Female">
        <label for="Female">Female</label><br>
        <input type="radio" name="sign_up" value="Other">
        <label for="Other">Other</label>
      
      <p>Nationality</p>
      <select name="Nationality">
        <option>Indonesia</option>
        <option>Amerika</option>
        <option>Inggris</option>
     </select >
     <p>Language Spoken</p>
    
        <input type="checkbox" id="BhsIndo" name="BhsIndo" value="BhsIndo">
        <label for="BhsIndo"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="Inggris" name="Inggris" value="Inggris">
        <label for="Inggris"> Inggris</label><br>
        <input type="checkbox" id="Other" name="Other" value="Other">
        <label for="Other"> Other</label>
	    <p>Bio</p>
        <textarea rows="7" cols="30"></textarea><br>
        <br>
	    <input type="submit" values="kirim">
      </form>   
    </div>
    @endsection